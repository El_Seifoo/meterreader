package com.meterreader.neomit.sief.meterreader.read;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;
import com.meterreader.neomit.sief.meterreader.utils.URLs;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sief on 7/15/2018.
 */

public class ReadingModel {


    protected void saveReading(Context context, DBCallback callback, Readings reading,boolean isEditingRejected) {
        boolean flag = ReadingsDbHelper.getmInstance(context).addReading(reading);
        callback.onResponse(context, flag,isEditingRejected);
    }

    protected void editReading(Context context, DBCallback callback, Readings reading) {
        boolean flag = ReadingsDbHelper.getmInstance(context).updateData(reading);
        callback.onResponse(context, reading, flag);
    }


    protected void retrieveReadings(final Context context, final VolleyCallback callback, final Readings reading) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.RETRIEVE_READING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response, 0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFail(context, error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<>();
                params.put("data[user_id]", MySingleton.getmInstance(context).getUserId());
                params.put("data[KEYS][0][meter_number]", reading.getMeterNumber().isEmpty() ? "-1" : reading.getMeterNumber());
                params.put("data[KEYS][0][meter_serial_number]", reading.getMeterSerialNumber().isEmpty() ? "-1" : reading.getMeterSerialNumber());
                params.put("data[KEYS][0][hcn_number]", reading.getHCNNumber().isEmpty() ? "-1" : reading.getHCNNumber());
                params.put("data[KEYS][0][plate_number]", reading.getPlateNumber().isEmpty() ? "-1" : reading.getPlateNumber());
                params.put("data[KEYS][0][dateFrom]", reading.getDate().split(" ")[0]);
                params.put("data[KEYS][0][dateTO]", reading.getDate().split(" ")[1]);

                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void locate(final Context context, final VolleyCallback callback, final Readings reading) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.MAP,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response, 1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFail(context, error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<>();
                params.put("data[0][meter_number]", reading.getMeterNumber().isEmpty() ? "-1" : reading.getMeterNumber());
                params.put("data[0][meter_serial_number]", reading.getMeterSerialNumber().isEmpty() ? "-1" : reading.getMeterSerialNumber());
                params.put("data[0][hcn_number]", reading.getHCNNumber().isEmpty() ? "-1" : reading.getHCNNumber());
                params.put("data[0][plate_number]", reading.getPlateNumber().isEmpty() ? "-1" : reading.getPlateNumber());
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getInfo(final Context context, final VolleyCallback callback, final Readings reading, final String lang) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.GET_INFO,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response, 2);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFail(context, error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<>();
                params.put("data[0][meter_number]", reading.getMeterNumber().isEmpty() ? "-1" : reading.getMeterNumber());
                params.put("data[0][meter_serial_number]", reading.getMeterSerialNumber().isEmpty() ? "-1" : reading.getMeterSerialNumber());
                params.put("data[0][hcn_number]", reading.getHCNNumber().isEmpty() ? "-1" : reading.getHCNNumber());
                params.put("data[0][plate_number]", reading.getPlateNumber().isEmpty() ? "-1" : reading.getPlateNumber());
                params.put("data[0][Lang]", lang);
                Log.e("params", params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected interface DBCallback {
        void onResponse(Context context, boolean flag,boolean isEditingRejected);

        void onResponse(Context context, Readings reading, boolean flag);
    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String response, int step) throws JSONException;

        void onFail(Context context, VolleyError error);
    }

}
