package com.meterreader.neomit.sief.meterreader.sync;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.read.ReadingActivity;
import com.meterreader.neomit.sief.meterreader.read.Readings;

import java.util.ArrayList;
import java.util.Collection;

public class SyncActivity extends AppCompatActivity implements SyncReadingsAdapter.ListItemClickListener, SyncMVP.View {
    private TextView emptyListTextView;
    private Button saveButton;
    private RecyclerView recyclerView;
    private SyncReadingsAdapter adapter;
    private ProgressBar progressBar;
    private EditText search;

    private SyncMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.readings));

        presenter = new SyncPresenter(this, new SyncModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        saveButton = (Button) findViewById(R.id.save_button);
        ((LinearLayout) findViewById(R.id.sync_btn_container)).setVisibility(getIntent().hasExtra("RetrievedReadings") ? View.GONE : View.VISIBLE);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestReadingsList(SyncActivity.this, true);
            }
        });
        recyclerView = (RecyclerView) findViewById(R.id.readings_recycler_view);
        recyclerView.setHasFixedSize(true);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            default:
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        }

        adapter = new SyncReadingsAdapter(this, getIntent().hasExtra("RetrievedReadings"));

        search = (EditText) findViewById(R.id.search_edit_text);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                presenter.requestSearch(readings, s.toString());

            }
        });
        presenter.preRequestReadingList(this, getIntent().hasExtra("RetrievedReadings"));
    }

    private final static int SCAN_QR_REQUEST = 50;

    public void scanQR(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
        Intent intent = integrator.createScanIntent();
        startActivityForResult(intent, SCAN_QR_REQUEST);
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyListText() {
        adapter.clear();
        emptyListTextView.setText(getString(R.string.no_readings_available));
    }

    private ArrayList<Readings> readings;

    @Override
    public void loadReadingsList(ArrayList<Readings> reading) {
        readings = new ArrayList<>();
        for (int i = 0 ; i < reading.size();i++){
            readings.add(reading.get(i));
        }
//        this.readings = reading;
        emptyListTextView.setText("");
        adapter.setReadings(reading);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateDBAfterSyncing(ArrayList<Readings> readings) {
        for (int i = 0; i < readings.size(); i++)
            presenter.requestRemoveReadingById(this, readings.get(i).getId());

    }

    @Override
    public void updateList(int id) {
        adapter.removeReading(this, id);
    }

    @Override
    public void notifyChanges() {
        presenter.preRequestReadingList(this, false);
    }

    @Override
    public void loadSearchedData(ArrayList<Readings> searchedList) {
        emptyListTextView.setText("");
        adapter.setReadings(searchedList);
    }

    //212.118.113.148:99

    private final static int EDIT_METER_READING_REQUEST = 10;

    @Override
    public void onListItemClick(Readings reading, View view) {
        switch (view.getId()) {
            case R.id.remove_reading:
                presenter.requestRemoveReadingById(this, reading.getId());
                break;
            default:
                Intent editIntent = new Intent(this, ReadingActivity.class);
                editIntent.putExtra("ReadingData", reading);
                startActivityForResult(editIntent, EDIT_METER_READING_REQUEST);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == EDIT_METER_READING_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.updateReading((Readings) data.getSerializableExtra("UpdatedReading"));
            return;
        }
        if (requestCode == SCAN_QR_REQUEST) {
            IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
            if (result != null) {
                if (result.getContents() != null) {
                    search.setText(result.getContents());
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
