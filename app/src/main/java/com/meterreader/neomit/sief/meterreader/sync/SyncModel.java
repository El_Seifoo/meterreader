package com.meterreader.neomit.sief.meterreader.sync;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.meterreader.neomit.sief.meterreader.read.Readings;
import com.meterreader.neomit.sief.meterreader.read.ReadingsDbHelper;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;
import com.meterreader.neomit.sief.meterreader.utils.URLs;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sief on 7/16/2018.
 */

public class SyncModel {

    /*
    { "meter_number": 7001, "meter_serial_number": 55, "hcn_number": 11, "plate_number": 11, "meter_reading": 400, "date": "2018-01-15 5:15" }
     */
    protected void getReadingsList(Context context, DBCallback callback, boolean isSyncing) {
        callback.onResponse(context, ReadingsDbHelper.getmInstance(context).getAllReadings(), isSyncing);
    }

    protected void removeReading(Context context, DBCallback callback, int id) {
        ReadingsDbHelper.getmInstance(context).removeById(id);
        callback.onRemovingSuccess(id);
    }

    public void updateAllReadings(Context context, DBCallback callback, ArrayList<Readings> rejected) {
        if (ReadingsDbHelper.getmInstance(context).updateDataList(rejected))
            callback.onUpdateSuccess();

    }

    protected void addReading(final Context context, final VolleyCallback callback, final ArrayList<Readings> readings) {
        Log.e("sss", MySingleton.getmInstance(context).getUserIP());
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.ADD_READING,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFail(context, error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new LinkedHashMap<>();
                params.put("data[user_id]", MySingleton.getmInstance(context).getUserId());
                for (int i = 0; i < readings.size(); i++) {
                    params.put("data[readings][" + i + "][meter_number]", readings.get(i).getMeterNumber().isEmpty() ? "-1" : readings.get(i).getMeterNumber());
                    params.put("data[readings][" + i + "][meter_serial_number]", readings.get(i).getMeterSerialNumber().isEmpty() ? "-1" : readings.get(i).getMeterSerialNumber());
                    params.put("data[readings][" + i + "][hcn_number]", readings.get(i).getHCNNumber().isEmpty() ? "-1" : readings.get(i).getHCNNumber());
                    params.put("data[readings][" + i + "][plate_number]", readings.get(i).getPlateNumber().isEmpty() ? "-1" : readings.get(i).getPlateNumber());
                    params.put("data[readings][" + i + "][meter_reading]", readings.get(i).getCurrentRead());
                    params.put("data[readings][" + i + "][date]", readings.get(i).getDate());
                    params.put("data[readings][" + i + "][reading_id]", readings.get(i).getId() + "");
                }

                Log.e("params", params.toString());
//                Log.e("data[user_id]", params.get("data[user_id]"));
//                for (int i = 0; i < params.size(); i++) {
//                    Log.e("data[readings][" + i + "1][meter_number]", params.get("data[readings][" + i + "][meter_number]"));
//                    Log.e("data[readings][" + i + "2][meter_serial_number]", params.get("data[readings][" + i + "][meter_serial_number]"));
//                    Log.e("data[readings][" + i + "3][hcn_number]", params.get("data[readings][" + i + "][hcn_number]"));
//                    Log.e("data[readings][" + i + "4][plate_number]", params.get("data[readings][" + i + "][plate_number]"));
//                    Log.e("data[readings][" + i + "5][meter_reading]", params.get("data[readings][" + i + "][meter_reading]"));
//                    Log.e("data[readings][" + i + "6][date]", params.get("data[readings][" + i + "][date]"));
//                    Log.e("data[readings][" + i + "7][reading_id]", params.get("data[readings][" + i + "][reading_id]"));
//                }
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected interface DBCallback {
        void onResponse(Context context, ArrayList<Readings> readings, boolean isSyncing);

        void onRemovingSuccess(int id);

        void onUpdateSuccess();
    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String response) throws JSONException;

        void onFail(Context context, VolleyError error);
    }
}
