package com.meterreader.neomit.sief.meterreader.sync;

import android.app.Activity;
import android.content.Context;

import com.meterreader.neomit.sief.meterreader.read.Readings;

import java.util.ArrayList;

/**
 * Created by sief on 7/16/2018.
 */

public interface SyncMVP {
    interface View {
        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void loadReadingsList(ArrayList<Readings> readings);

        void showToastMessage(String message);

        void updateDBAfterSyncing(ArrayList<Readings> readings);

        void updateList(int id);

        void notifyChanges();

        void loadSearchedData(ArrayList<Readings> searchedList);
    }

    interface Presenter {
        void preRequestReadingList(Activity activity, boolean isRetrieving);

        void requestReadingsList(Context context, boolean isSyncing);

        void requestRemoveReadingById(Context context, int id);

        void requestSearch(ArrayList<Readings> readings, String query);
    }
}
