package com.meterreader.neomit.sief.meterreader.sync;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.read.Readings;

import java.util.ArrayList;

/**
 * Created by sief on 7/16/2018.
 */

public class SyncReadingsAdapter extends RecyclerView.Adapter<SyncReadingsAdapter.Holder> {
    ArrayList<Readings> readings;
    final private ListItemClickListener mOnClickListener;
    private boolean isRetrieving;

    public SyncReadingsAdapter(ListItemClickListener mOnClickListener, boolean isRetrieving) {
        this.mOnClickListener = mOnClickListener;
        this.isRetrieving = isRetrieving;
    }

    public void clear() {
        if (readings != null) {
            readings.clear();
            notifyDataSetChanged();
        }
    }

    public interface ListItemClickListener {
        void onListItemClick(Readings reading, View view);
    }

    public void setReadings(ArrayList<Readings> readings) {
        this.readings = readings;
        notifyDataSetChanged();
    }

    public void removeReading(Context context, int id) {
        if (readings != null && readings.size() > 0) {
            for (int i = 0; i < readings.size(); i++) {
                if (readings.get(i).getId() == id) {
                    readings.remove(i);
                    notifyItemRemoved(i);
                    break;
                }
            }
        }
        if (readings != null && readings.isEmpty()) {
            ((SyncActivity) context).showEmptyListText();
        }
    }

    public void updateReading(Readings reading) {
        for (int i = 0; i < readings.size(); i++) {
            if (reading.getId() == readings.get(i).getId()) {
                readings.set(i, reading);
                notifyDataSetChanged();
                break;
            }
        }
    }

    @NonNull
    @Override
    public SyncReadingsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.readings_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SyncReadingsAdapter.Holder holder, int position) {

        String allNumbers = "";
        if (!readings.get(position).getMeterNumber().isEmpty() && !readings.get(position).getMeterNumber().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.meter_number) + ": </font>" + readings.get(position).getMeterNumber() + "<br>";
        if (!readings.get(position).getMeterSerialNumber().isEmpty() && !readings.get(position).getMeterSerialNumber().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.meter_serial_number) + ": </font>" + readings.get(position).getMeterSerialNumber() + "<br>";
        if (!readings.get(position).getHCNNumber().isEmpty() && !readings.get(position).getHCNNumber().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.hcn_number) + ": </font>" + readings.get(position).getHCNNumber() + "<br>";
        if (!readings.get(position).getPlateNumber().isEmpty() && !readings.get(position).getPlateNumber().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.plate_number) + ": </font>" + readings.get(position).getPlateNumber() + "<br>";

        if (!readings.get(position).getCurrentRead().isEmpty() && !readings.get(position).getCurrentRead().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.meter_current_read) + ": </font>" + readings.get(position).getCurrentRead() + "<br>";

        if (readings.get(position).getReason() != null)
            if (!readings.get(position).getReason().isEmpty() && !readings.get(position).getReason().toLowerCase().equals("null"))
                allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.error) + ": </font>" + readings.get(position).getReason() + "<br>";

        holder.meterNumbers.setText(Html.fromHtml(allNumbers.substring(0, allNumbers.length() - 4)), TextView.BufferType.SPANNABLE);
//        holder.meterReading.setText(Html.fromHtml("<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.meter_current_read) + ": </font>" + readings.get(position).getCurrentRead()), TextView.BufferType.SPANNABLE);
        holder.date.setText(Html.fromHtml("<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.date) + ": </font>" + readings.get(position).getDate()), TextView.BufferType.SPANNABLE);
        holder.removeReading.setVisibility(isRetrieving ? View.GONE : View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return readings != null ? readings.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView meterNumbers, meterReading, date;
        ImageView removeReading;

        public Holder(View itemView) {
            super(itemView);
            meterNumbers = (TextView) itemView.findViewById(R.id.meter_numbers);
            meterReading = (TextView) itemView.findViewById(R.id.meter_readings);
            date = (TextView) itemView.findViewById(R.id.date);
            removeReading = (ImageView) itemView.findViewById(R.id.remove_reading);
            itemView.setOnClickListener(isRetrieving ? null : this);
            removeReading.setOnClickListener(isRetrieving ? null : this);
        }

        @Override
        public void onClick(View view) {
            mOnClickListener.onListItemClick(readings.get(getAdapterPosition()), view);
        }
    }
}
