package com.meterreader.neomit.sief.meterreader.read;

import java.io.Serializable;

/**
 * Created by sief on 7/15/2018.
 */

public class Readings implements Serializable {
    private int id;
    private String readerID;
    private String meterNumber;
    private String meterSerialNumber;
    private String HCNNumber;
    private String plateNumber;
    private String currentRead;
    private String date;
    private String reason;


    public Readings(int id, String readerID, String meterNumber,
                    String meterSerialNumber, String HCNNumber, String plateNumber, String currentRead, String date, String reason) {
        this.id = id;
        this.readerID = readerID;
        this.meterNumber = meterNumber;
        this.meterSerialNumber = meterSerialNumber;
        this.HCNNumber = HCNNumber;
        this.plateNumber = plateNumber;
        this.currentRead = currentRead;
        this.date = date;
        this.reason = reason;
    }

    public Readings(String readerID, String meterNumber,
                    String meterSerialNumber, String HCNNumber, String plateNumber, String currentRead, String date) {
        this.readerID = readerID;
        this.meterNumber = meterNumber;
        this.meterSerialNumber = meterSerialNumber;
        this.HCNNumber = HCNNumber;
        this.plateNumber = plateNumber;
        this.currentRead = currentRead;
        this.date = date;
    }

    public Readings(int id, String meterNumber, String meterSerialNumber, String HCNNumber,
                    String plateNumber, String currentRead, String date) {
        this.id = id;
        this.meterNumber = meterNumber;
        this.meterSerialNumber = meterSerialNumber;
        this.HCNNumber = HCNNumber;
        this.plateNumber = plateNumber;
        this.currentRead = currentRead;
        this.date = date;
    }

    public Readings(int id, String meterNumber, String meterSerialNumber, String HCNNumber,
                    String plateNumber, String currentRead, String date, String reason) {
        this.id = id;
        this.meterNumber = meterNumber;
        this.meterSerialNumber = meterSerialNumber;
        this.HCNNumber = HCNNumber;
        this.plateNumber = plateNumber;
        this.currentRead = currentRead;
        this.date = date;
        this.reason = reason;
    }

    public Readings(String meterNumber, String meterSerialNumber, String HCNNumber, String plateNumber, String currentRead, String date) {
        this.meterNumber = meterNumber;
        this.meterSerialNumber = meterSerialNumber;
        this.HCNNumber = HCNNumber;
        this.plateNumber = plateNumber;
        this.currentRead = currentRead;
        this.date = date;
    }


    public Readings(String fake, String meterNumber, String meterSerialNumber, String plateNumber, String HCNNumber, String currentRead, String date, String reason) {
        this.meterNumber = meterNumber;
        this.meterSerialNumber = meterSerialNumber;
        this.plateNumber = plateNumber;
        this.HCNNumber = HCNNumber;
        this.currentRead = currentRead;
        this.date = date;
        this.reason = reason;
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getReaderID() {
        return readerID;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public String getMeterSerialNumber() {
        return meterSerialNumber;
    }

    public String getHCNNumber() {
        return HCNNumber;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public String getCurrentRead() {
        return currentRead;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public String getReason() {
        return reason;
    }

}
