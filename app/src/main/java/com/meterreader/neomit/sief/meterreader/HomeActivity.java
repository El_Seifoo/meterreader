package com.meterreader.neomit.sief.meterreader;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.meterreader.neomit.sief.meterreader.Login.LoginActivity;
import com.meterreader.neomit.sief.meterreader.read.ReadingActivity;
import com.meterreader.neomit.sief.meterreader.read.ReadingsDbHelper;
import com.meterreader.neomit.sief.meterreader.rejected.RejectedActivity;
import com.meterreader.neomit.sief.meterreader.sync.SyncActivity;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setTitle(getString(R.string.home));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        int screenSize = getResources().getConfiguration().screenLayout &
//                Configuration.SCREENLAYOUT_SIZE_MASK;
//        switch (screenSize) {
//            case Configuration.SCREENLAYOUT_SIZE_SMALL:
//                Toast.makeText(this, "small", Toast.LENGTH_SHORT).show();
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
//                Toast.makeText(this, "normal", Toast.LENGTH_SHORT).show();
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
//                Toast.makeText(this, "Xlarge", Toast.LENGTH_SHORT).show();
//                break;
//            case Configuration.SCREENLAYOUT_SIZE_LARGE:
//                Toast.makeText(this, "Large", Toast.LENGTH_SHORT).show();
//                break;
//            default:
//        }
        /////////////////////////////////
//        resizePhoto((ImageView) findViewById(R.id.add_button));
//        resizePhoto((ImageView) findViewById(R.id.retrieve_button));
//        resizePhoto((ImageView) findViewById(R.id.offline_button));
//        resizePhoto((ImageView) findViewById(R.id.map_button));
//        resizePhoto((ImageView) findViewById(R.id.get_info_button));
//        resizePhoto((ImageView) findViewById(R.id.logout_button));

        /////////////////////////////////
    }

    private void resizePhoto(final ImageView imageView) {
        ViewTreeObserver vto = imageView.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = imageView.getMeasuredHeight();
                int finalWidth = imageView.getMeasuredWidth();
                Log.e("width,height", finalWidth + " === " + finalHeight);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, finalWidth);
                imageView.setLayoutParams(params);
                return true;
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_button:
                try {
                    startActivity(new Intent(getApplicationContext(), ReadingActivity.class));
                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.retrieve_button:
                try {
                    Intent intent = new Intent(getApplicationContext(), ReadingActivity.class);
                    intent.putExtra("Retrieving", "Retrieving");
                    startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.offline_button:
                try {
                    startActivity(new Intent(getApplicationContext(), SyncActivity.class));
                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.map_button:
                try {
                    Intent intent2 = new Intent(getApplicationContext(), ReadingActivity.class);
                    intent2.putExtra("Location", "Location");
                    startActivity(intent2);
                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.get_info_button:
                try {

                } catch (Exception e) {
                    Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
                }
                Intent intent3 = new Intent(getApplicationContext(), ReadingActivity.class);
                intent3.putExtra("GetInfo", "GetInfo");
                startActivity(intent3);
                break;
            case R.id.rejected_button:
                startActivity(new Intent(getApplicationContext(), RejectedActivity.class));
                break;
            case R.id.logout_button:
                String ip = MySingleton.getmInstance(this).getUserIP();
                MySingleton.getmInstance(this).logout();
//                ReadingsDbHelper.getmInstance(this).deleteAll();
                MySingleton.getmInstance(this).saveUserIp(ip);
                Intent intent1 = new Intent(this, LoginActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.version_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
