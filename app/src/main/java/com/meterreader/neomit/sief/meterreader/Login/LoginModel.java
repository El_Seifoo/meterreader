package com.meterreader.neomit.sief.meterreader.Login;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;
import com.meterreader.neomit.sief.meterreader.utils.URLs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sief on 7/16/2018.
 */

public class LoginModel {

    protected void login(final Context context, final VolleyCallback callback, final String id, final String password, final String ip) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                URLs.HTTP + ip + URLs.LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        callback.onSuccess(context, response, id, ip);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFail(context, error);
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("data[user_id]", id);
                params.put("data[password]", password);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);

    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String response, String userId, String ip);

        void onFail(Context context, VolleyError error);
    }
}
