package com.meterreader.neomit.sief.meterreader.sync;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.read.Readings;
import com.meterreader.neomit.sief.meterreader.read.ReadingsDbHelper;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sief on 7/16/2018.
 */

public class SyncPresenter implements SyncMVP.Presenter, SyncModel.DBCallback, SyncModel.VolleyCallback {
    private SyncMVP.View view;
    private SyncModel model;

    public SyncPresenter(SyncMVP.View view, SyncModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void preRequestReadingList(Activity activity, boolean isRetrieving) {
        if (isRetrieving)
            view.loadReadingsList((ArrayList<Readings>) activity.getIntent().getSerializableExtra("RetrievedReadings"));
        else this.requestReadingsList(activity, false);
    }

    @Override
    public void requestReadingsList(Context context, boolean isSyncing) {
        model.getReadingsList(context, this, isSyncing);
    }

    @Override
    public void requestRemoveReadingById(Context context, int id) {
        model.removeReading(context, this, id);
    }

    @Override
    public void requestSearch(ArrayList<Readings> readings, String query) {

        if (readings == null) return;
        if (readings.isEmpty()) return;
        if (query.isEmpty()) {
            view.loadReadingsList(readings);
            return;
        }


        ArrayList<Readings> searchedList = new ArrayList<>();
        for (int i = 0; i < readings.size(); i++) {
            if (readings.get(i).getPlateNumber().startsWith(query) || readings.get(i).getMeterSerialNumber().startsWith(query) || readings.get(i).getMeterNumber().startsWith(query))
                searchedList.add(readings.get(i));
        }

        if (searchedList.isEmpty())
            view.showEmptyListText();
        else
            view.loadSearchedData(searchedList);

        Log.e("aaaa", query + ").....");
        Log.e("aaaa", "size: " + readings.size());
    }

    @Override
    public void onResponse(Context context, ArrayList<Readings> readings, boolean isSyncing) {
        if (readings.isEmpty()) {
            if (isSyncing)
                view.showToastMessage(context.getString(R.string.no_readings_to_sync));
            else
                view.showEmptyListText();
        } else {
            if (isSyncing) {
                view.showProgress();
                model.addReading(context, this, readings);
            } else
                view.loadReadingsList(readings);
        }
    }

    @Override
    public void onRemovingSuccess(int id) {
        view.updateList(id);
    }

    @Override
    public void onUpdateSuccess() {
        view.notifyChanges();
    }

    @Override
    public void onSuccess(Context context, String response) throws JSONException {
        view.hideProgress();
        JSONObject jsonObject = new JSONObject(response);
        ArrayList<Readings> readingsList = new ArrayList<>();
        ArrayList<Readings> rejected = new ArrayList<>();
        JSONArray readings = jsonObject.getJSONArray("readings");
        for (int i = 0; i < readings.length(); i++) {
            /*
            int id, String meterNumber, String meterSerialNumber, String HCNNumber,
                    String plateNumber, String currentRead, String date
             */
            if (readings.getJSONObject(i).getString("is_error").equals("false"))
                readingsList.add(new Readings(readings.getJSONObject(i).getInt("reading_id"), readings.getJSONObject(i).getString("meter_number"),
                        readings.getJSONObject(i).getString("meter_serial_number"), readings.getJSONObject(i).getString("hcn_number"), readings.getJSONObject(i).getString("plate_number"),
                        readings.getJSONObject(i).getString("meter_reading"), readings.getJSONObject(i).getString("date")));
            else
                rejected.add(new Readings(readings.getJSONObject(i).getInt("reading_id"), readings.getJSONObject(i).getString("meter_number"),
                        readings.getJSONObject(i).getString("meter_serial_number"), readings.getJSONObject(i).getString("hcn_number"), readings.getJSONObject(i).getString("plate_number"),
                        readings.getJSONObject(i).getString("meter_reading"), readings.getJSONObject(i).getString("date"), readings.getJSONObject(i).getString("ErrorMessage")));
        }

        if (readingsList.isEmpty() || (readingsList.size() != ReadingsDbHelper.getmInstance(context).getReadingsCount())) {
            view.showToastMessage(context.getString(R.string.failed_to_sync_data_check_your_data));
            if (!rejected.isEmpty())
                model.updateAllReadings(context, this, rejected);
        } else {
            view.showToastMessage(context.getString(R.string.sync_done_successfully));
            view.updateDBAfterSyncing(readingsList);
        }

    }

    @Override
    public void onFail(Context context, VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));
    }
}
