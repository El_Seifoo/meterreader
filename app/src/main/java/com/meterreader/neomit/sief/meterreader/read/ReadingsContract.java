package com.meterreader.neomit.sief.meterreader.read;

import android.provider.BaseColumns;

/**
 * Created by sief on 7/15/2018.
 */

public class ReadingsContract {
    /*
      String id, String readerName, String readerID, String meterNumber,
                    String meterSerialNumber, String HCNNumber, String plateNumber, String currentRead, String date
     */
    public class ReadingsEntry implements BaseColumns {
        public static final String TABLE_NAME = "readingsValues";
        public static final String ID = "id";
        public static final String READER_ID = "readerID";
        public static final String METER_NUMBER = "meterNumber";
        public static final String METER_SERIAL_NUMBER = "meterSerialNumber";
        public static final String HCN_NUMBER = "HCNNumber";
        public static final String PLATE_NUMBER = "plateNumber";
        public static final String CURRENT_READ = "currentRead";
        public static final String DATE = "date";
        public static final String REASON = "reason";

    }
}
