package com.meterreader.neomit.sief.meterreader.rejected;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.read.ReadingActivity;
import com.meterreader.neomit.sief.meterreader.read.Readings;

import java.util.ArrayList;

public class RejectedActivity extends AppCompatActivity implements RejectedMVP.View, RejectedAdapter.ListItemClickListener {
    private static final int EDIT_REJECTED_OBJECT_REQUEST = 1;
    private TextView emptyListTextView;
    private RecyclerView recyclerView;
    private RejectedAdapter adapter;
    private ProgressBar progressBar;

    private RejectedMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rejected);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.rejected));

        presenter = new RejectedPresenter(this, new RejectedModel());

        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);

        recyclerView = (RecyclerView) findViewById(R.id.readings_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new RejectedAdapter(this);

        presenter.requestReadingsList();

    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getBContext() {
        return getBaseContext();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyListText() {
        emptyListTextView.setText(getString(R.string.no_readings_available));
    }

    @Override
    public void loadRejectedList(ArrayList<Readings> list) {
        emptyListTextView.setText("");
        adapter.setList(list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onListItemClick(int position, Readings reading) {
        Intent intent = new Intent(this, ReadingActivity.class);
        intent.putExtra("Edit Rejected", reading);
        intent.putExtra("Position", position);
        startActivityForResult(intent, EDIT_REJECTED_OBJECT_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_REJECTED_OBJECT_REQUEST && resultCode == RESULT_OK && data != null) {
            adapter.removeReading(this, data.getIntExtra("Position", 0));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
