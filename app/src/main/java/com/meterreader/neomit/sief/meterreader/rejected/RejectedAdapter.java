package com.meterreader.neomit.sief.meterreader.rejected;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.read.Readings;

import java.util.ArrayList;

public class RejectedAdapter extends RecyclerView.Adapter<RejectedAdapter.Holder> {
    private ArrayList<Readings> list;
    final private ListItemClickListener mOnClickListener;

    public RejectedAdapter(ListItemClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }

    public void setList(ArrayList<Readings> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public interface ListItemClickListener {
        void onListItemClick(int position, Readings reading);
    }

    public void clear() {
        if (list != null) {
            list.clear();
            notifyDataSetChanged();
        }
    }

    public void removeReading(Context context, int position) {
        if (list != null) {
            list.remove(position);
            notifyItemRemoved(position);
            if (list.isEmpty())
                ((RejectedActivity) context).showEmptyListText();
        }
    }

    @NonNull
    @Override
    public RejectedAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rejected_reading_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RejectedAdapter.Holder holder, int position) {
        String allNumbers = "";
        if (!list.get(position).getMeterNumber().isEmpty() && !list.get(position).getMeterNumber().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.meter_number) + ": </font>" + list.get(position).getMeterNumber() + "<br>";
        if (!list.get(position).getMeterSerialNumber().isEmpty() && !list.get(position).getMeterSerialNumber().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.meter_serial_number) + ": </font>" + list.get(position).getMeterSerialNumber() + "<br>";
        if (!list.get(position).getHCNNumber().isEmpty() && !list.get(position).getHCNNumber().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.hcn_number) + ": </font>" + list.get(position).getHCNNumber() + "<br>";
        if (!list.get(position).getPlateNumber().isEmpty() && !list.get(position).getPlateNumber().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.plate_number) + ": </font>" + list.get(position).getPlateNumber() + "<br>";

        if (!list.get(position).getCurrentRead().isEmpty() && !list.get(position).getCurrentRead().equals("-1"))
            allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.meter_current_read) + ": </font>" + list.get(position).getCurrentRead() + "<br>";

        if (list.get(position).getReason() != null)
            if (!list.get(position).getReason().isEmpty() && !list.get(position).getReason().toLowerCase().equals("null"))
                allNumbers += "<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.error) + ": </font>" + list.get(position).getReason() + "<br>";

        holder.meterNumbers.setText(Html.fromHtml(allNumbers.substring(0, allNumbers.length() - 4)), TextView.BufferType.SPANNABLE);
//        holder.meterReading.setText(Html.fromHtml("<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.meter_current_read) + ": </font>" + list.get(position).getCurrentRead()), TextView.BufferType.SPANNABLE);
        holder.date.setText(Html.fromHtml("<font color = '#84AF2C'>" + holder.itemView.getContext().getString(R.string.date) + ": </font>" + list.get(position).getDate()), TextView.BufferType.SPANNABLE);


    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView meterNumbers, meterReading, date;

        public Holder(@NonNull View itemView) {
            super(itemView);
            meterNumbers = (TextView) itemView.findViewById(R.id.meter_numbers);
            meterReading = (TextView) itemView.findViewById(R.id.meter_readings);
            date = (TextView) itemView.findViewById(R.id.date);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            mOnClickListener.onListItemClick(getAdapterPosition(), list.get(getAdapterPosition()));
        }
    }
}
