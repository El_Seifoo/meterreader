package com.meterreader.neomit.sief.meterreader.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.meterreader.neomit.sief.meterreader.R;


/**
 * Created by sief on 5/13/2018.
 */

public class MySingleton extends Application {
    private static MySingleton mInstance;
    private RequestQueue requestQueue;
    private static Context context;
    private SharedPreferences sharedPreferences;

    //  Agency  {address=abou youssef, city_id=1768, password=1020, longitude=2222, funding_services=1, latitude=1111, email=agency@gmail.com, name=agency, phone=012012, valuating_services=1, type=agency, country_id=242, commercial_number=123}
    //  User {"city_id":"1768","password":"1020","email":"individual@gmail.com","name":"individual","phone":"011011","birthday":"2018-05-23","type":"individual","country_id":"242"}
    private MySingleton(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
        sharedPreferences = getSharedPreferences();
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }

        return requestQueue;
    }

    public static synchronized MySingleton getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySingleton(context);
        }

        return mInstance;
    }


    public void addToRQ(Request request) {
        requestQueue.add(request);
    }


    public SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    /*
     *  Saving user token
     *  take one param (token : String)
     *  save token using sharedPref
     */
    public void saveUserId(String userId) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(Constants.USER_TOKEN, userId);
        editor.apply();
    }/*
     *  Saving user ip
     *  take one param (userIp : String)
     *  save token using sharedPref
     */

    public void saveUserIp(String userIp) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(Constants.USER_IP, userIp);
        editor.apply();
    }

    /*
     *  Getting userToken
     */
    public String getUserId() {
        return sharedPreferences.getString(Constants.USER_TOKEN, "");
    }

    /*
     *  Getting userToken
     */
    public String getUserIP() {
        return sharedPreferences.getString(Constants.USER_IP, "");
    }


    /*
     *  saving that user logged in
     */
    public void loginUser() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(Constants.IS_LOGGED_IN, true);
        editor.apply();
    }

    /*
     *  Check if user is logged in or not
     */
    public boolean isLoggedIn() {
        return getSharedPreferences().getBoolean(Constants.IS_LOGGED_IN, false);
    }

    /*
     *  Logging the user out by deleting all
     *  user data from sharedPref
     */
    public void logout() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.clear();
        editor.apply();
    }
}
