package com.meterreader.neomit.sief.meterreader.read;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.meterreader.neomit.sief.meterreader.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

/**
 * Created by sief on 7/15/2018.
 */

public class ReadingPresenter implements ReadingMVP.Presenter, ReadingModel.DBCallback, ReadingModel.VolleyCallback {
    private ReadingMVP.View view;
    private ReadingModel model;

    public ReadingPresenter(ReadingMVP.View view, ReadingModel model) {
        this.view = view;
        this.model = model;
    }


    @Override
    public void requestReadingData(Activity activity, boolean isEditing) {
        if (!isEditing) return;
        view.loadReadingData((Readings) activity.getIntent().getSerializableExtra("ReadingData"));
    }

    @Override
    public void requestSaveData(Context context, Readings reading, boolean isEditing, boolean isRetrieving, boolean isLocating, boolean isGettingInfo, String lang) {
        if ((reading.getMeterNumber().isEmpty() && reading.getMeterSerialNumber().isEmpty() &&
                reading.getHCNNumber().isEmpty() && reading.getPlateNumber().isEmpty())) {
            view.showToastMessage(context.getString(R.string.must_record_one_of_four_fields));
            return;
        }


        if (isEditing) {
            if (reading.getCurrentRead().isEmpty() || view.getTime().isEmpty() || reading.getDate().isEmpty()) {
                view.showToastMessage(context.getString(R.string.must_record_reading_date_and_time));
                return;
            }
            reading.setId(view.getReadingId());
            reading.setDate(reading.getDate() + " " + view.getTime());
            model.editReading(context, this, reading);
        } else if (isLocating) {
            view.showProgress();
            model.locate(context, this, reading);
        } else if (isGettingInfo) {
            view.showProgress();
            model.getInfo(context, this, reading, lang.equals(context.getString(R.string.settings_language_english_label)) ? "En" : "Ar");
        } else {
            if (isRetrieving) {
                if (reading.getDate().isEmpty() || view.getDateTo().isEmpty()) {
                    view.showToastMessage(context.getString(R.string.must_record_reading_date_and_end_date));
                    return;
                }
                Calendar startDate = Calendar.getInstance();
                startDate.set(Integer.valueOf(reading.getDate().split("/")[0]), (Integer.valueOf(reading.getDate().split("/")[1]) - 1), Integer.valueOf(reading.getDate().split("/")[2]));
                Calendar endDate = Calendar.getInstance();
                endDate.set(Integer.valueOf(view.getDateTo().split("/")[0]), (Integer.valueOf(view.getDateTo().split("/")[1]) - 1), Integer.valueOf(view.getDateTo().split("/")[2]));
                if (startDate.after(endDate)) {
                    view.showToastMessage(context.getString(R.string.check_start_and_end_date));
                    return;
                }
                view.showProgress();
                reading.setDate(reading.getDate() + " " + view.getDateTo());
                model.retrieveReadings(context, this, reading);
            } else {
                if (reading.getCurrentRead().isEmpty() || view.getTime().isEmpty() || reading.getDate().isEmpty()) {
                    view.showToastMessage(context.getString(R.string.must_record_reading_date_and_time));
                    return;
                }
                reading.setDate(reading.getDate() + " " + view.getTime());
                model.saveReading(context, this, reading, false);
            }
        }
    }

    @Override
    public void requestSaveData(Context context, Readings reading) {
        if ((reading.getMeterNumber().isEmpty() && reading.getMeterSerialNumber().isEmpty() &&
                reading.getHCNNumber().isEmpty() && reading.getPlateNumber().isEmpty())) {
            view.showToastMessage(context.getString(R.string.must_record_one_of_four_fields));
            return;
        }

        if (reading.getCurrentRead().isEmpty() || view.getTime().isEmpty() || reading.getDate().isEmpty()) {
            view.showToastMessage(context.getString(R.string.must_record_reading_date_and_time));
            return;
        }
        reading.setDate(reading.getDate() + " " + view.getTime());
        model.saveReading(context, this, reading, true);
    }

    @Override
    public void requestQRScanner(Activity activity, String editText) {
        IntentIntegrator integrator = new IntentIntegrator(activity);
        Collection<String> ss = new ArrayList<>();
        ss.addAll(IntentIntegrator.ONE_D_CODE_TYPES);
        ss.addAll(IntentIntegrator.QR_CODE_TYPES);
        ss.addAll(IntentIntegrator.PRODUCT_CODE_TYPES);
        ss.addAll(IntentIntegrator.DATA_MATRIX_TYPES);
        integrator.setDesiredBarcodeFormats(ss);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        int flag = 0;
        if (editText.equals("meterNumber")) {
            flag = 0;
        } else if (editText.equals("meterSerialNumber")) {
            flag = 1;
        } else if (editText.equals("HCNNumber")) {
            flag = 2;
        } else if (editText.equals("plateNumber")) {
            flag = 3;
        }
        view.initializeScanner(integrator, flag);
    }

    @Override
    public void handleQRScannerResult(String data, int flag, EditText[] editTexts) {
        if (flag == 3)
            view.setQRData(editTexts[flag], data.replaceAll("[^0-9]", ""));
        else
            view.setQRData(editTexts[flag], data);

    }

    @Override
    public void requestDatePicker(Context context, final EditText editText) {
        Calendar calendar = Calendar.getInstance();
        String dateString = editText.getText().toString().trim();
        if (!dateString.equals("")) {
            calendar.set(Integer.valueOf(dateString.split("/")[0]), (Integer.valueOf(dateString.split("/")[1]) - 1), Integer.valueOf(dateString.split("/")[2]));
        }
        DatePickerDialog date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        context,
                        R.style.DatePickerDialogStyle),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view1, int year, int month, int dayOfMonth) {
                        Calendar birthDay = Calendar.getInstance();
                        birthDay.set(Calendar.YEAR, year);
                        birthDay.set(Calendar.MONTH, month);
                        birthDay.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        // update edit text with selected date
                        String myFormat = "yyyy/MM/dd";
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        String DOB = sdf.format(birthDay.getTime());
//                        view.setText(DOB);
                        view.setData(editText, DOB);
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }

    @Override
    public void requestTimePicker(Context context, final EditText editText) {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new CustomTimePickerDialog(context, TimePickerDialog.THEME_HOLO_LIGHT,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view1, int hourOfDay, int minute) {
                        String hourString = hourOfDay + "";
                        String minString = minute + "";
                        if (hourOfDay < 10) hourString = "0" + hourString;
                        if (minute < 10) minString = "0" + minString;
                        view.setData(editText, hourString + ":" + minString);
                    }
                }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.setCanceledOnTouchOutside(false);
        timePickerDialog.show();
    }

    @Override
    public void onResponse(Context context, boolean flag, boolean isEditingRejected) {
        if (flag) {
            view.onSaveReadingSuccess(context.getString(R.string.reading_saved_successfully));
            if (isEditingRejected)
                view.OnEditRejectedSuccess();
        } else {
            view.onSaveReadingFail();
            if (isEditingRejected)
                view.onEditRejectedFailed();
        }
    }

    @Override
    public void onResponse(Context context, Readings reading, boolean flag) {
        if (flag)
            view.onEditReadingSuccess(context.getString(R.string.reading_edited_successfully), reading);
        else
            view.onSaveReadingFail();
    }

    @Override
    public void onSuccess(Context context, String response, int step) throws JSONException {
        view.hideProgress();
        if (step == 0) {
            JSONObject jsonObject = new JSONObject(response);
            ArrayList<Readings> readingsList = new ArrayList<>();
            JSONArray readings = jsonObject.getJSONArray("data");
            String date;
            for (int i = 0; i < readings.length(); i++) {
                date = readings.getJSONObject(i).getString("READ_DATE");
                readingsList.add(new Readings(readings.getJSONObject(i).getString("METER_NO"), readings.getJSONObject(i).getString("Serial_No"),
                        readings.getJSONObject(i).getString("HCN"), readings.getJSONObject(i).getString("Plate_No"), readings.getJSONObject(i).getString("READING"),
                        date.split("T")[0] + " " + date.split("T")[1].split(":")[0] + ":" + date.split("T")[1].split(":")[1]));
            }
            if (readingsList.isEmpty())
                view.showToastMessage(context.getString(R.string.no_available_readings_for_this_search));
            else view.goReadingListView(readingsList);
        } else if (step == 1) {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray readings = jsonObject.getJSONArray("data");
            if (readings.length() > 0 && readings.getJSONObject(0).get("X_Coordinate") != null && readings.getJSONObject(0).get("Y_Coordinate") != null) {
                double longitude = readings.getJSONObject(0).getDouble("X_Coordinate");
                double latitude = readings.getJSONObject(0).getDouble("Y_Coordinate");
                Log.e("lon,lat", longitude + " === " + latitude);
                view.onLocatingMeterSuccess(longitude, latitude);
            } else
                view.showToastMessage(context.getString(R.string.no_location_for_this_meter));
        } else if (step == 2) {
            ArrayList<Info> infoList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray readings = jsonObject.getJSONArray("data");
            if (readings.length() > 0) {

                infoList.add(new Info(context.getString(R.string.distance_to_main_line), returnValidData(readings.getJSONObject(0).getString("DISTANCE_TO_MAIN_LINE"))));
                infoList.add(new Info(context.getString(R.string.depth_main_line), returnValidData(readings.getJSONObject(0).getString("DEPTH_MAIN_LINE"))));
                infoList.add(new Info(context.getString(R.string.diameter_main_line), returnValidData(readings.getJSONObject(0).getString("DIAMETER_MAIN_LINE"))));
                infoList.add(new Info(context.getString(R.string.material_main_line), returnValidData(readings.getJSONObject(0).getString("MATERIAL_MAIN_LINE"))));
                infoList.add(new Info(context.getString(R.string.meter_number), returnValidData(readings.getJSONObject(0).getString("METER_NO"))));
                infoList.add(new Info(context.getString(R.string.account_number), returnValidData(readings.getJSONObject(0).getString("AccountNumber"))));
                infoList.add(new Info(context.getString(R.string.serial_number), returnValidData(readings.getJSONObject(0).getString("Serial_No"))));
                infoList.add(new Info(context.getString(R.string.first_name), returnValidData(readings.getJSONObject(0).getString("FirstName"))));
                infoList.add(new Info(context.getString(R.string.mid_name), returnValidData(readings.getJSONObject(0).getString("MidName"))));
                infoList.add(new Info(context.getString(R.string.family_name), returnValidData(readings.getJSONObject(0).getString("FamilyName"))));
                infoList.add(new Info(context.getString(R.string.arabic_name), returnValidData(readings.getJSONObject(0).getString("ArabicName"))));
                infoList.add(new Info(context.getString(R.string.building_number), returnValidData(readings.getJSONObject(0).getString("Building_No"))));
                infoList.add(new Info(context.getString(R.string.building_dup), returnValidData(readings.getJSONObject(0).getString("Building_Dup"))));
                infoList.add(new Info(context.getString(R.string.phone_number), returnValidData(readings.getJSONObject(0).getString("Phone_No"))));
                infoList.add(new Info(context.getString(R.string.email), returnValidData(readings.getJSONObject(0).getString("Email"))));
                infoList.add(new Info(context.getString(R.string.address), returnValidData(readings.getJSONObject(0).getString("Address"))));
                infoList.add(new Info(context.getString(R.string.post_code), returnValidData(readings.getJSONObject(0).getString("Post_Code"))));
                infoList.add(new Info(context.getString(R.string.pipe_status), returnValidData(readings.getJSONObject(0).getString("Pipe_After_Meter_Id"))));
                infoList.add(new Info(context.getString(R.string.reducer_diameter), returnValidData(readings.getJSONObject(0).getString("REDUCER_DIAMETER"))));
                infoList.add(new Info(context.getString(R.string.pipe_size), returnValidData(readings.getJSONObject(0).getString("PIPE_SIZES"))));
                infoList.add(new Info(context.getString(R.string.pipe_material), returnValidData(readings.getJSONObject(0).getString("PIPE_MATERIAL"))));
                infoList.add(new Info(context.getString(R.string.valve_status), returnValidData(readings.getJSONObject(0).getString("VALVE_STATUS"))));
                infoList.add(new Info(context.getString(R.string.sub_building_type), returnValidData(readings.getJSONObject(0).getString("SUB_BUILDING_TYPE"))));
                infoList.add(new Info(context.getString(R.string.meter_status), returnValidData(readings.getJSONObject(0).getString("METER_STATUS"))));
                infoList.add(new Info(context.getString(R.string.meter_remarks), returnValidData(readings.getJSONObject(0).getString("METER_REMARKS"))));
                infoList.add(new Info(context.getString(R.string.localized_districts), returnValidData(readings.getJSONObject(0).getString("LOCALIZED_DISTRICTS"))));
                infoList.add(new Info(context.getString(R.string.meter_position), returnValidData(readings.getJSONObject(0).getString("METER_POSITION"))));
                infoList.add(new Info(context.getString(R.string.meter_location), returnValidData(readings.getJSONObject(0).getString("METER_LOCATION"))));
                infoList.add(new Info(context.getString(R.string.meter_box_type), returnValidData(readings.getJSONObject(0).getString("METER_BOX_TYPE"))));
                infoList.add(new Info(context.getString(R.string.valve_type), returnValidData(readings.getJSONObject(0).getString("VALVE_TYPE"))));
                infoList.add(new Info(context.getString(R.string.read_type), returnValidData(readings.getJSONObject(0).getString("READ_TYPE"))));
                infoList.add(new Info(context.getString(R.string.customer_type), returnValidData(readings.getJSONObject(0).getString("CUSTOMER_TYPE"))));
                infoList.add(new Info(context.getString(R.string.water_connection_type), returnValidData(readings.getJSONObject(0).getString("WATER_CONNECTION_TYPE"))));
                infoList.add(new Info(context.getString(R.string.meter_material), returnValidData(readings.getJSONObject(0).getString("METER_MATERIAL"))));
                infoList.add(new Info(context.getString(R.string.classification_level_3), returnValidData(readings.getJSONObject(0).getString("CLASSIFICATION_LEVEL3"))));
                infoList.add(new Info(context.getString(R.string.building_type), returnValidData(readings.getJSONObject(0).getString("BUILDING_TYPE"))));
                infoList.add(new Info(context.getString(R.string.city), returnValidData(readings.getJSONObject(0).getString("CITY"))));
                infoList.add(new Info(context.getString(R.string.region), returnValidData(readings.getJSONObject(0).getString("REGION"))));
//                infoList.add(new Info(context.getString(R.string.name_ar), returnValidData(readings.getJSONObject(0).getString("NAME_AR"))));
                //name_en , name_ar
                infoList.add(new Info(context.getString(R.string.cover_status), returnValidData(readings.getJSONObject(0).getString("Cover_Status"))));
                infoList.add(new Info(context.getString(R.string.plate_number), returnValidData(readings.getJSONObject(0).getString("Plate_No"))));
                infoList.add(new Info(context.getString(R.string.hcn_number), returnValidData(readings.getJSONObject(0).getString("HCN"))));
                infoList.add(new Info(context.getString(R.string.x_coordinate), returnValidData(readings.getJSONObject(0).getString("X_Coordinate"))));
                infoList.add(new Info(context.getString(R.string.y_coordinate), returnValidData(readings.getJSONObject(0).getString("Y_Coordinate"))));
                infoList.add(new Info(context.getString(R.string.seco_number), returnValidData(readings.getJSONObject(0).getString("SECO_No"))));
                infoList.add(new Info(context.getString(R.string.is_sewer_connection_exist), returnValidData(readings.getJSONObject(0).getString("IsSewerConnectionExist"))));
                infoList.add(new Info(context.getString(R.string.number_of_electric_metres), returnValidData(readings.getJSONObject(0).getString("NumberOfElectricMetres"))));
                infoList.add(new Info(context.getString(R.string.elevation), returnValidData(readings.getJSONObject(0).getString("Elevation"))));
                infoList.add(new Info(context.getString(R.string.ground_elevation), returnValidData(readings.getJSONObject(0).getString("Ground_Elevation"))));
//                infoList.add(new Info(context.getString(R.string.building_line_number), returnValidData(readings.getJSONObject(0).getString("Building_Line_No"))));
                infoList.add(new Info(context.getString(R.string.node_number), returnValidData(readings.getJSONObject(0).getString("Node_No"))));
                infoList.add(new Info(context.getString(R.string.number_units), returnValidData(readings.getJSONObject(0).getString("Number_Units"))));
                infoList.add(new Info(context.getString(R.string.electrical_meter_count), returnValidData(readings.getJSONObject(0).getString("Electrical_Meter_Count"))));
                infoList.add(new Info(context.getString(R.string.install_date), returnValidData(readings.getJSONObject(0).getString("Install_Date"))));
                infoList.add(new Info(context.getString(R.string.description), returnValidData(readings.getJSONObject(0).getString("Description"))));
                infoList.add(new Info(context.getString(R.string.water_con_rate_id), returnValidData(readings.getJSONObject(0).getString("Water_Con_Rate_Id"))));
                infoList.add(new Info(context.getString(R.string.est_reading), returnValidData(readings.getJSONObject(0).getString("Est_Reading"))));
                infoList.add(new Info(context.getString(R.string.authentication_number), returnValidData(readings.getJSONObject(0).getString("Authen_No"))));
                infoList.add(new Info(context.getString(R.string.water_connection_type_id), returnValidData(readings.getJSONObject(0).getString("Water_Connection_Type_Id"))));
                infoList.add(new Info(context.getString(R.string.sub_district_id), returnValidData(readings.getJSONObject(0).getString("SUB_DISTRICT_ID"))));
//                infoList.add(new Info(context.getString(R.string.is_blocked), returnValidData(readings.getJSONObject(0).getString("IS_BLOCKED"))));
//                infoList.add(new Info(context.getString(R.string.is_active), returnValidData(readings.getJSONObject(0).getString("IS_ACTIVE"))));
                view.loadInfoData(infoList);
            } else {
                view.showToastMessage(context.getString(R.string.no_available_information_for_this_meter));
            }
        }
    }

    private String returnValidData(String data) {
        return data.trim().equals("null") ? "" : data.trim();
    }

    @Override
    public void onFail(Context context, VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));
    }
}
