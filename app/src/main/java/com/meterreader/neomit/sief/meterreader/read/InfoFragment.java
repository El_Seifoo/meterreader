package com.meterreader.neomit.sief.meterreader.read;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.meterreader.neomit.sief.meterreader.R;

import java.util.ArrayList;

/**
 * Created by sief on 7/24/2018.
 */

public class InfoFragment extends Fragment {

    /*
    *  Static method to initialize fragment
    *  no params
    *  return SendMailFragment
    */
    public static InfoFragment newInstance(ArrayList<Info> infoList) {
        InfoFragment fragment = new InfoFragment();

        /*
         *  but intent data to args if newInstance
         *  has params
         */
        Bundle args = new Bundle();
        args.putSerializable("InfoList", infoList);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_get_info, container, false);
        ArrayList<Info> infoList = (ArrayList<Info>) getArguments().getSerializable("InfoList");
        TextView mainLabel = (TextView) view.findViewById(R.id.main_label);
        mainLabel.setText("All information about Meter:....." + infoList.get(4).getValue() + ".....");
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.info_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        InfoAdapter adapter = new InfoAdapter(infoList);
        recyclerView.setAdapter(adapter);
        return view;
    }
}
