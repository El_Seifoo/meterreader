package com.meterreader.neomit.sief.meterreader.rejected;

import android.content.Context;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.read.Readings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RejectedPresenter implements RejectedMVP.Presenter, RejectedModel.VolleyCallback {
    private RejectedMVP.View view;
    private RejectedModel model;

    public RejectedPresenter(RejectedMVP.View view, RejectedModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestReadingsList() {
        view.showProgress();
        model.getRejected(view.getBContext(), this);
    }

    @Override
    public void onSuccess(Context context, String responseString) throws JSONException {
        view.hideProgress();
        JSONObject response = new JSONObject(responseString);
        if (response.getString("IsError").toLowerCase().equals("true")) {
            view.showEmptyListText();
        } else {
            JSONArray data = response.getJSONArray("data");
            if (data.length() == 0) {
                view.showEmptyListText();
            } else {
                ArrayList<Readings> list = new ArrayList<>();
                for (int i = 0; i < data.length(); i++) {
                    list.add(new Readings("",
                            returnValidString(data.getJSONObject(i).getString("METER_NO")),
                            returnValidString(data.getJSONObject(i).getString("meter_serial_number")),
                            returnValidString(data.getJSONObject(i).getString("Plate_No")),
                            returnValidString(data.getJSONObject(i).getString("HCN")),
                            returnValidString(data.getJSONObject(i).getString("LastRead")),
                            returnValidDate(data.getJSONObject(i).getString("LastReadDate")),
                            returnValidString(data.getJSONObject(i).getString("Reason"))));
                }

                view.loadRejectedList(list);
            }
        }
    }

    private String returnValidString(String data) {
        return data.toLowerCase().equals("null") || data.equals("N/A") || data.isEmpty() || data.equals("-1") ? "" : data;
    }

    private String returnValidDate(String data) {
        if (data.toLowerCase().equals("null") || data.equals("N/A") || data.isEmpty()) return "";
        if (!data.toLowerCase().contains("t")) return "";
        //yyyy/MM/dd hh:mm
        String date = data.toLowerCase().split("t")[0];
        String time = data.toLowerCase().split("t")[1];
        date = date.replace("-", "/");
        time = time.split(":")[0] + ":" + time.split(":")[1];
        return date.concat(" ").concat(time);
    }

    @Override
    public void onFailed(Context context, VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection_or_your_host_address_is_wrong));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));
    }
}
