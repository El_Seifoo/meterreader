package com.meterreader.neomit.sief.meterreader.rejected;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;
import com.meterreader.neomit.sief.meterreader.utils.URLs;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class RejectedModel {

    protected void getRejected(final Context context, final VolleyCallback callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.HTTP + MySingleton.getmInstance(context).getUserIP() + URLs.REJECTED,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response", response);
                        try {
                            callback.onSuccess(context, response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFailed(context, error);
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected interface VolleyCallback {
        void onSuccess(Context context, String responseString) throws JSONException;

        void onFailed(Context context, VolleyError error);
    }
}
