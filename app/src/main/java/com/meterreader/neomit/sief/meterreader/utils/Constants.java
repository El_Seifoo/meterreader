package com.meterreader.neomit.sief.meterreader.utils;

/**
 * Created by sief on 5/13/2018.
 */

public class Constants {
    public static final String SHARED_PREF_NAME = "meterReader";
    public static final String USER_TOKEN = "token";
    public static final String USER_IP = "ip";
    public static final String IS_LOGGED_IN = "is_logged_in";

}
