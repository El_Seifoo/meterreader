package com.meterreader.neomit.sief.meterreader.read;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by sief on 7/15/2018.
 */

public class ReadingsDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "readings";
    private static final int DATABASE_VERSION = 7;
    private static ReadingsDbHelper mInstance;

    private ReadingsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized ReadingsDbHelper getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new ReadingsDbHelper(context);
        }
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_READING_TABLE = "CREATE TABLE " + ReadingsContract.ReadingsEntry.TABLE_NAME + " ( " +
                ReadingsContract.ReadingsEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ReadingsContract.ReadingsEntry.READER_ID + " TEXT, " +
                ReadingsContract.ReadingsEntry.METER_NUMBER + " TEXT, " +
                ReadingsContract.ReadingsEntry.METER_SERIAL_NUMBER + " TEXT, " +
                ReadingsContract.ReadingsEntry.HCN_NUMBER + " TEXT, " +
                ReadingsContract.ReadingsEntry.PLATE_NUMBER + " TEXT, " +
                ReadingsContract.ReadingsEntry.CURRENT_READ + " TEXT, " +
                ReadingsContract.ReadingsEntry.DATE + " TEXT, " +
                ReadingsContract.ReadingsEntry.REASON + " TEXT" +
                " );";
        db.execSQL(SQL_CREATE_READING_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ReadingsContract.ReadingsEntry.TABLE_NAME);
        onCreate(db);
    }

    public boolean addReading(Readings reading) {
        if (reading == null) return false;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ReadingsContract.ReadingsEntry.READER_ID, reading.getReaderID());
        values.put(ReadingsContract.ReadingsEntry.METER_NUMBER, reading.getMeterNumber());
        values.put(ReadingsContract.ReadingsEntry.METER_SERIAL_NUMBER, reading.getMeterSerialNumber());
        values.put(ReadingsContract.ReadingsEntry.HCN_NUMBER, reading.getHCNNumber());
        values.put(ReadingsContract.ReadingsEntry.PLATE_NUMBER, reading.getPlateNumber());
        values.put(ReadingsContract.ReadingsEntry.CURRENT_READ, reading.getCurrentRead());
        values.put(ReadingsContract.ReadingsEntry.DATE, reading.getDate());
        if (reading.getReason() != null)
            values.put(ReadingsContract.ReadingsEntry.REASON, reading.getReason());

        db.insert(ReadingsContract.ReadingsEntry.TABLE_NAME, ReadingsContract.ReadingsEntry.REASON, values);
        db.close();
        return true;
    }

    public boolean updateData(Readings reading) {
        if (reading == null) return false;
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();
        values.put(ReadingsContract.ReadingsEntry.ID, reading.getId());
        values.put(ReadingsContract.ReadingsEntry.READER_ID, reading.getReaderID());
        values.put(ReadingsContract.ReadingsEntry.METER_NUMBER, reading.getMeterNumber());
        values.put(ReadingsContract.ReadingsEntry.METER_SERIAL_NUMBER, reading.getMeterSerialNumber());
        values.put(ReadingsContract.ReadingsEntry.HCN_NUMBER, reading.getHCNNumber());
        values.put(ReadingsContract.ReadingsEntry.PLATE_NUMBER, reading.getPlateNumber());
        values.put(ReadingsContract.ReadingsEntry.CURRENT_READ, reading.getCurrentRead());
        values.put(ReadingsContract.ReadingsEntry.DATE, reading.getDate());
        if (reading.getReason() != null)
            values.put(ReadingsContract.ReadingsEntry.REASON, reading.getReason());

        db.update(ReadingsContract.ReadingsEntry.TABLE_NAME, values, ReadingsContract.ReadingsEntry.ID + " = ?",
                new String[]{String.valueOf(reading.getId())});

        db.close();
        return true;
    }

    public boolean updateDataList(ArrayList<Readings> readings) {
        if (readings == null) return false;


        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values;
        for (int i = 0; i < readings.size(); i++) {
            values = new ContentValues();
            values.put(ReadingsContract.ReadingsEntry.ID, readings.get(i).getId());
            values.put(ReadingsContract.ReadingsEntry.READER_ID, readings.get(i).getReaderID());
            values.put(ReadingsContract.ReadingsEntry.METER_NUMBER, readings.get(i).getMeterNumber());
            values.put(ReadingsContract.ReadingsEntry.METER_SERIAL_NUMBER, readings.get(i).getMeterSerialNumber());
            values.put(ReadingsContract.ReadingsEntry.HCN_NUMBER, readings.get(i).getHCNNumber());
            values.put(ReadingsContract.ReadingsEntry.PLATE_NUMBER, readings.get(i).getPlateNumber());
            values.put(ReadingsContract.ReadingsEntry.CURRENT_READ, readings.get(i).getCurrentRead());
            values.put(ReadingsContract.ReadingsEntry.DATE, readings.get(i).getDate());
            if (readings.get(i).getReason() != null)
                values.put(ReadingsContract.ReadingsEntry.REASON, readings.get(i).getReason());

            db.update(ReadingsContract.ReadingsEntry.TABLE_NAME, values, ReadingsContract.ReadingsEntry.ID + " = ?",
                    new String[]{String.valueOf(readings.get(i).getId())});
        }
        db.close();

        return true;
    }

    public ArrayList<Readings> getAllReadings() {
        ArrayList<Readings> readings = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + ReadingsContract.ReadingsEntry.TABLE_NAME + " ORDER BY " +
                ReadingsContract.ReadingsEntry.ID + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                /*
                    String id,String readerID, String meterNumber,
                    String meterSerialNumber, String HCNNumber, String plateNumber, String currentRead, String date
                 */
                Readings reading = new Readings(cursor.getInt(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.ID)),
                        cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.READER_ID)), cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.METER_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.METER_SERIAL_NUMBER)), cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.HCN_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.PLATE_NUMBER)), cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.CURRENT_READ)),
                        cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.DATE)), cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.REASON)));
                readings.add(reading);

            } while (cursor.moveToNext());
        }

        db.close();

        return readings;
    }

    public int getReadingsCount() {
        ArrayList<Readings> readings = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + ReadingsContract.ReadingsEntry.TABLE_NAME + " ORDER BY " +
                ReadingsContract.ReadingsEntry.ID + " ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                /*
                    String id,String readerID, String meterNumber,
                    String meterSerialNumber, String HCNNumber, String plateNumber, String currentRead, String date
                 */
                Readings reading = new Readings(cursor.getInt(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.ID)),
                        cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.READER_ID)), cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.METER_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.METER_SERIAL_NUMBER)), cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.HCN_NUMBER)),
                        cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.PLATE_NUMBER)), cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.CURRENT_READ)),
                        cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.DATE)), cursor.getString(cursor.getColumnIndex(ReadingsContract.ReadingsEntry.REASON)));
                readings.add(reading);

            } while (cursor.moveToNext());
        }

        db.close();

        return readings.size();
    }

    public void removeById(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ReadingsContract.ReadingsEntry.TABLE_NAME, ReadingsContract.ReadingsEntry.ID + " = ?",
                new String[]{String.valueOf(id)});
        db.close();
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + ReadingsContract.ReadingsEntry.TABLE_NAME);
        db.close();
    }
}
