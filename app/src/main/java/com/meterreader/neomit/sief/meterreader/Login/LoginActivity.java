package com.meterreader.neomit.sief.meterreader.Login;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.meterreader.neomit.sief.meterreader.HomeActivity;
import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.read.ReadingActivity;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;

public class LoginActivity extends AppCompatActivity implements LoginMVP.View {
    private EditText username, password, ip;
    private LoginMVP.Presenter presenter;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle(getString(R.string.logi_in));

        presenter = new LoginPresenter(this, new LoginModel());
        presenter.checkIfUserLoggedIn(MySingleton.getmInstance(this).isLoggedIn());


        progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

        username = (EditText) findViewById(R.id.user_name_edit_text);
        password = (EditText) findViewById(R.id.password_edit_text);
        ip = (EditText) findViewById(R.id.ip_edit_text);
        ip.setText(MySingleton.getmInstance(this).getUserIP());
        ((Button) findViewById(R.id.login_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.requestLogin(LoginActivity.this, username.getText().toString().trim(),
                        password.getText().toString().trim(), ip.getText().toString().trim());
            }
        });

    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goHome() {
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
