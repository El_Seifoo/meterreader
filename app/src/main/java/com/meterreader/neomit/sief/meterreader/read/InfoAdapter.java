package com.meterreader.neomit.sief.meterreader.read;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.meterreader.neomit.sief.meterreader.R;

import java.util.ArrayList;

/**
 * Created by sief on 7/24/2018.
 */

public class InfoAdapter extends RecyclerView.Adapter<InfoAdapter.Holder> {
    ArrayList<Info> infoList;

    public InfoAdapter(ArrayList<Info> infoList) {
        this.infoList = infoList;
    }

    @NonNull
    @Override
    public InfoAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.get_info_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull InfoAdapter.Holder holder, int position) {
        holder.textView.setText(infoList.get(position).getLabel());
        holder.editText.setText(infoList.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return infoList != null ? infoList.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView textView;
        private EditText editText;

        public Holder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.label);
            editText = (EditText) itemView.findViewById(R.id.edit_text);
        }
    }
}
