package com.meterreader.neomit.sief.meterreader.Login;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.UnknownHostException;

/**
 * Created by sief on 7/16/2018.
 */

public class LoginPresenter implements LoginMVP.Presenter, LoginModel.VolleyCallback {
    private LoginMVP.View view;
    private LoginModel model;

    public LoginPresenter(LoginMVP.View view, LoginModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void checkIfUserLoggedIn(boolean isLoggedIn) {
        if (!isLoggedIn) return;
        view.goHome();
    }

    @Override
    public void requestLogin(Context context, String username, String password, String ip) {
        if (username.isEmpty() || password.isEmpty() || ip.isEmpty()) {
            view.showToastMessage(context.getString(R.string.please_fill_all_fields));
            return;
        }
        view.showProgress();
        model.login(context, this, username, password, ip);
    }

    @Override
    public void onSuccess(Context context, String response, String userId, String ip) {
        view.hideProgress();
        if (response.toLowerCase().trim().equals("true")) {
            MySingleton.getmInstance(context).saveUserId(userId);
            MySingleton.getmInstance(context).saveUserIp(ip);
            MySingleton.getmInstance(context).loginUser();
            view.goHome();
        } else view.showToastMessage(context.getString(R.string.invalid_username_or_password));

    }

    @Override
    public void onFail(Context context, VolleyError error) {
        view.hideProgress();
        if (error instanceof AuthFailureError) {
            view.showToastMessage(context.getString(R.string.auth_failure_error));
            return;
        }
        if (error instanceof NoConnectionError) {
            view.showToastMessage(context.getString(R.string.no_internet_connection_or_your_host_address_is_wrong));
            return;
        }
        if (error instanceof TimeoutError) {
            view.showToastMessage(context.getString(R.string.time_out_error_message));
            return;
        }
        if (error instanceof ServerError) {
            view.showToastMessage(context.getString(R.string.server_error));
            return;
        }
        view.showToastMessage(context.getString(R.string.wrong_message));
    }
}
