package com.meterreader.neomit.sief.meterreader.utils;

/**
 * Created by sief on 5/13/2018.
 */

public class URLs {

    public static final String HTTP = "http://";
    public static final String ROOT_URL = "/api/reading/";
    public static final String LOGIN = ROOT_URL + "login";
    public static final String ADD_READING = ROOT_URL + "add";
    public static final String RETRIEVE_READING = ROOT_URL + "Retrieve";
    public static final String MAP = ROOT_URL + "Map";
    public static final String GET_INFO = ROOT_URL + "GetInfo";
    public static final String REJECTED = ROOT_URL + "GetRejected";
}
