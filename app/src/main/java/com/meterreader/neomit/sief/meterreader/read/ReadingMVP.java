package com.meterreader.neomit.sief.meterreader.read;

import android.app.Activity;
import android.content.Context;
import android.widget.EditText;

import com.google.zxing.integration.android.IntentIntegrator;

import java.util.ArrayList;

/**
 * Created by sief on 7/15/2018.
 */

public interface ReadingMVP {
    interface View {
        void loadReadingData(Readings reading);

        void initializeScanner(IntentIntegrator intentIntegrator, int flag);

        void setQRData(EditText editText, String data);

        void showProgress();

        void hideProgress();

        void showToastMessage(String message);

        void onSaveReadingSuccess(String message);

        int getReadingId();

        String getTime();

        String getDateTo();

        void onEditReadingSuccess(String message, Readings reading);

        void onSaveReadingFail();

        void setData(EditText view, String data);

        void goReadingListView(ArrayList<Readings> readings);

        void onLocatingMeterSuccess(double longitude, double latitude);

        void loadInfoData(ArrayList<Info> infoList);

        void OnEditRejectedSuccess();

        void onEditRejectedFailed();
    }

    interface Presenter {
        void requestReadingData(Activity activity, boolean isEditing);

        void requestSaveData(Context context, Readings reading, boolean isEditing, boolean isRetrieving, boolean isLocating, boolean isGettingInfo, String lang);

        void requestSaveData(Context context, Readings reading);

        void requestQRScanner(Activity activity, String editText);

        void handleQRScannerResult(String data, int flag, EditText[] editTexts);

        void requestDatePicker(Context context, EditText view);

        void requestTimePicker(Context context, EditText view);
    }
}
