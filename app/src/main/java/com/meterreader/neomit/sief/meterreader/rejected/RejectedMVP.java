package com.meterreader.neomit.sief.meterreader.rejected;

import android.content.Context;

import com.meterreader.neomit.sief.meterreader.read.Readings;

import java.util.ArrayList;

public interface RejectedMVP {
    interface View {
        Context getAppContext();

        Context getBContext();

        void showProgress();

        void hideProgress();

        void showEmptyListText();

        void loadRejectedList(ArrayList<Readings> list);

        void showToastMessage(String message);
    }

    interface Presenter {
        void requestReadingsList();
    }
}
