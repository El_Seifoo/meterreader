package com.meterreader.neomit.sief.meterreader.read;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.meterreader.neomit.sief.meterreader.R;
import com.meterreader.neomit.sief.meterreader.sync.SyncActivity;
import com.meterreader.neomit.sief.meterreader.utils.MySingleton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ReadingActivity extends AppCompatActivity implements ReadingMVP.View {
    private EditText meterNumber, meterSerialNumber, HCNNumber, plateNumber, meterCurrentRead, date, dateTo, time;
    private Button saveButton;
    private FrameLayout fragmentContainer;
    private ProgressBar progressBar;

    private ReadingMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(
                    getIntent().hasExtra("ReadingData") ? getString(R.string.edit_reading) :
                            getIntent().hasExtra("Retrieving") ? getString(R.string.retrieve) :
                                    getIntent().hasExtra("Location") ? getString(R.string.meter_location) :
                                            getIntent().hasExtra("GetInfo") ? getString(R.string.get_info) :
                                                    getIntent().hasExtra("Edit Rejected") ? getString(R.string.edit_rejected) :
                                                            getString(R.string.add_reading));

            presenter = new ReadingPresenter(this, new ReadingModel());

            fragmentContainer = (FrameLayout) findViewById(R.id.frame);

            progressBar = (ProgressBar) findViewById(R.id.loading_spinner);

            meterNumber = (EditText) findViewById(R.id.meter_number_edit_text);
            meterSerialNumber = (EditText) findViewById(R.id.meter_serial_number_edit_text);
            HCNNumber = (EditText) findViewById(R.id.hcn_number_edit_text);
            plateNumber = (EditText) findViewById(R.id.plate_number_edit_text);
            meterCurrentRead = (EditText) findViewById(R.id.meter_current_read_edit_text);
            date = (EditText) findViewById(R.id.date_edit_text);
            time = (EditText) findViewById(R.id.time_edit_text);
            Calendar today = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd kk:mm");
            String stringDate = sdf.format(today.getTime());
            date.setText(stringDate.split(" ")[0]);
            time.setText(stringDate.split(" ")[1]);
            saveButton = (Button) findViewById(R.id.save_button);

            if (getIntent().hasExtra("Retrieving")) {
                date.setText("");
                date.setHint(R.string.date_from);
                dateTo = (EditText) findViewById(R.id.date_to_edit_text);
                dateTo.setVisibility(View.VISIBLE);
                time.setText("");
                time.setVisibility(View.GONE);
                ((LinearLayout) findViewById(R.id.meter_current_read_container)).setVisibility(View.GONE);
                saveButton.setText(getString(R.string.retrieve));
            } else if (getIntent().hasExtra("Location")) {
                ((LinearLayout) findViewById(R.id.date_time_container)).setVisibility(View.GONE);
                ((LinearLayout) findViewById(R.id.meter_current_read_container)).setVisibility(View.GONE);
                saveButton.setText(getString(R.string.meter_location));
            } else if (getIntent().hasExtra("GetInfo")) {
                ((LinearLayout) findViewById(R.id.date_time_container)).setVisibility(View.GONE);
                ((LinearLayout) findViewById(R.id.meter_current_read_container)).setVisibility(View.GONE);
                RadioGroup radioGroup = (RadioGroup) findViewById(R.id.language_group_container);
                radioGroup.setVisibility(View.VISIBLE);
                ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
                saveButton.setText(getString(R.string.get_info));
            } else if (getIntent().hasExtra("Edit Rejected")) {
                loadReadingData((Readings) getIntent().getSerializableExtra("Edit Rejected"));
            }

            // check if user editing meterReading or not ..
            presenter.requestReadingData(this, getIntent().hasExtra("ReadingData"));


            saveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getIntent().hasExtra("Edit Rejected")) {
                        presenter.requestSaveData(ReadingActivity.this, new Readings(MySingleton.getmInstance(ReadingActivity.this).getUserId(), meterNumber.getText().toString().trim(),
                                meterSerialNumber.getText().toString().trim(), HCNNumber.getText().toString().trim(), plateNumber.getText().toString().trim(),
                                meterCurrentRead.getText().toString().trim(), date.getText().toString().trim()));
                    } else
                        presenter.requestSaveData(ReadingActivity.this, new Readings(MySingleton.getmInstance(ReadingActivity.this).getUserId(), meterNumber.getText().toString().trim(),
                                        meterSerialNumber.getText().toString().trim(), HCNNumber.getText().toString().trim(), plateNumber.getText().toString().trim(),
                                        meterCurrentRead.getText().toString().trim(), date.getText().toString().trim()),
                                getIntent().hasExtra("ReadingData"), getIntent().hasExtra("Retrieving"), getIntent().hasExtra("Location"), getIntent().hasExtra("GetInfo"), getIntent().hasExtra("GetInfo") ? getLang(((RadioGroup) findViewById(R.id.language_group_container))) : "");
                }
            });


        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }


//        resizePhoto((ImageView) findViewById(R.id.meter_number_qr));
//        resizePhoto((ImageView) findViewById(R.id.meter_serial_number_qr));
//        resizePhoto((ImageView) findViewById(R.id.hcn_number_qr));
//        resizePhoto((ImageView) findViewById(R.id.plate_number_qr));

    }

    public static String getLang(RadioGroup radioGroup) {
        int selected = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = (RadioButton) radioGroup.findViewById(selected);
        if (radioButton == null) return null;
        return radioButton.getText().toString();
    }

    private void resizePhoto(final ImageView imageView) {
        ViewTreeObserver vto = imageView.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
//                int finalHeight = imageView.getMeasuredHeight();
                int finalWidth = imageView.getMeasuredWidth();
                Log.e("width:", finalWidth + "");
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int) (finalWidth * 1.5), finalWidth);
                imageView.setLayoutParams(params);
                return true;
            }
        });
    }

    public void scanQR(View view) {
        switch (view.getId()) {
            case R.id.meter_number_qr:
                presenter.requestQRScanner(this, "meterNumber");
                break;
            case R.id.meter_serial_number_qr:
                presenter.requestQRScanner(this, "meterSerialNumber");
                break;
            case R.id.hcn_number_qr:
                presenter.requestQRScanner(this, "HCNNumber");
                break;
            case R.id.plate_number_qr:
                presenter.requestQRScanner(this, "plateNumber");
        }
    }

    public void showDateDialog(final View view) {
        presenter.requestDatePicker(this, ((EditText) view));
    }

    public void showTimePicker(final View view) {
        presenter.requestTimePicker(this, ((EditText) view));
    }

    @Override
    public void loadReadingData(Readings reading) {
        meterNumber.setText(reading.getMeterNumber().equals("-1") ? "" : reading.getMeterNumber());
        meterSerialNumber.setText(reading.getMeterSerialNumber().equals("-1") ? "" : reading.getMeterSerialNumber());
        HCNNumber.setText(reading.getHCNNumber().equals("-1") ? "" : reading.getHCNNumber());
        plateNumber.setText(reading.getPlateNumber().equals("-1") ? "" : reading.getPlateNumber());
        if (!getIntent().hasExtra("Edit Rejected")){
            meterCurrentRead.setText(reading.getCurrentRead().equals("-1") ? "" : reading.getCurrentRead());
            date.setText(reading.getDate().split(" ")[0]);
            time.setText(reading.getDate().split(" ")[1]);
        }
    }

    @Override
    public void initializeScanner(IntentIntegrator intentIntegrator, int flag) {
        Intent intent = intentIntegrator.createScanIntent();
        startActivityForResult(intent, flag);
    }

    @Override
    public void setQRData(EditText editText, String data) {
        editText.setText(data);
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSaveReadingSuccess(String message) {
        showToastMessage(message);
        meterNumber.setText("");
        meterSerialNumber.setText("");
        HCNNumber.setText("");
        plateNumber.setText("");
        meterCurrentRead.setText("");
        date.setText("");
        time.setText("");
    }

    @Override
    public int getReadingId() {
        return ((Readings) getIntent().getSerializableExtra("ReadingData")).getId();
    }

    @Override
    public String getTime() {
        return time.getText().toString().trim();
    }

    @Override
    public String getDateTo() {
        return dateTo.getText().toString().trim();
    }

    @Override
    public void onEditReadingSuccess(String message, Readings reading) {
        showToastMessage(message);
        Intent returnToReadingsList = new Intent();
        returnToReadingsList.putExtra("UpdatedReading", reading);
        setResult(RESULT_OK, returnToReadingsList);
        finish();
    }

    @Override
    public void onSaveReadingFail() {
        showToastMessage(getString(R.string.failed_to_save_reading_try_again));
    }

    @Override
    public void setData(EditText view, String data) {
        view.setText(data);
    }

    @Override
    public void goReadingListView(ArrayList<Readings> readings) {
        Intent intent = new Intent(this, SyncActivity.class);
        intent.putExtra("RetrievedReadings", readings);
        startActivity(intent);
    }

    @Override
    public void onLocatingMeterSuccess(double longitude, double latitude) {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?daddr=" + longitude + "," + latitude));
        startActivity(intent);
    }

    @Override
    public void loadInfoData(ArrayList<Info> infoList) {
        fragmentContainer.setClickable(true);
        Fragment fragmentMail = InfoFragment.newInstance(infoList);
        if (fragmentMail != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frame, fragmentMail, "Info");
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void OnEditRejectedSuccess() {
        Intent returnToRejectedList = new Intent();
        returnToRejectedList.putExtra("Position", getIntent().getIntExtra("Position", 0));
        setResult(RESULT_OK, returnToRejectedList);
        finish();
    }

    @Override
    public void onEditRejectedFailed() {
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(IntentIntegrator.REQUEST_CODE, resultCode, data);
        if (result != null) {
            if (result.getContents() != null)
                presenter.handleQRScannerResult(result.getContents(), requestCode, new EditText[]{meterNumber, meterSerialNumber, HCNNumber, plateNumber});
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Fragment child = (InfoFragment) getSupportFragmentManager().findFragmentByTag("Info");
            if (child != null) {
                getSupportFragmentManager().beginTransaction().
                        remove(getSupportFragmentManager().findFragmentByTag("Info")).commit();
                fragmentContainer.setClickable(false);
                return true;
            }
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment child = (InfoFragment) getSupportFragmentManager().findFragmentByTag("Info");
        if (child != null) {
            getSupportFragmentManager().beginTransaction().
                    remove(getSupportFragmentManager().findFragmentByTag("Info")).commit();
            fragmentContainer.setClickable(false);
            return;
        }
        super.onBackPressed();
    }
}
