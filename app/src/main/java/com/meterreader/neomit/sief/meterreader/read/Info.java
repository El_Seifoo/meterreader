package com.meterreader.neomit.sief.meterreader.read;

import java.io.Serializable;

/**
 * Created by sief on 7/24/2018.
 */

public class Info implements Serializable {
    private String label;
    private String value;

    public Info(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }
}
